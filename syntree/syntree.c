#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "syntree.h"

/*
// Gruppe 20, Compilerbau - Monobachelor Informatik
// Anar Aliyev      (617945)
// Angelique Anders (613527)
*/

extern int syntreeInit(Syntree *self){
    self->allocated = 2;
    Syntree *tree = (Syntree *) malloc(self->allocated * sizeof(Syntree));
    if (tree == NULL) {
        fprintf(stderr, "Speicherallokation ist fehlgeschlagen!");
        exit(1);
    }
    self->child = tree;
    self->value = -1;
    return 0;    
}

void syntreeRelease(Syntree *self){
    if (self->size == 0)
        return;
    int i=0;
    while (i < self->size) {
        free(&self->child);
        i++;
    }    
}

SyntreeNodeID syntreeNodeNumber(Syntree *self, int number){
    SyntreeNodeID id = number;
    return id;
}

SyntreeNodeID syntreeNodeTag(Syntree *self, SyntreeNodeID id){
    self->size++;
    Syntree *child = &self[0];
    child->value = id;
    child->size = 0;
    child->allocated = 0;
    return id;
}
void syntreeNodePairHelfer(Syntree *self, SyntreeNodeID id1, SyntreeNodeID id2) {
    if (self->size == 0) {
        return;
    }
    int index = -1;
    for (int i = 0; i < self->size; i++) {
        if (self[i].value == id2) {
            index = i;
            break;
        }
    }
    if (index != -1) {
        int allocate = 2;
        Syntree *tree = (Syntree *) malloc(allocate * sizeof(Syntree));
        if (tree == NULL) {
            fprintf(stderr, "Speicherallozieren ist fehlgeschlagen");
            exit(1);
        }

        tree->child = self->child;
        tree->value = id1;
        tree->size = self->size;
        tree->allocated = self->allocated;
        self->child = tree;
        self->allocated = allocate;
        self->size = 1;
        return;
    }
    for (int i = 0; i < self->size; i++) {
        if (self[i].size != 0)
            syntreeNodePairHelfer(&self[i], id1, id2);
    }
}
SyntreeNodeID syntreeNodePair(Syntree *self, SyntreeNodeID id1, SyntreeNodeID id2){
    syntreeNodePairHelfer(self, id1, id2);
    return id1;
}

void insert(Syntree *self, SyntreeNodeID elem, int pos) {
    for (int i = self->size; i >= pos; i--)
        self[i] = self[i - 1];
    Syntree *child = &self[pos];
    child->value = elem;
    child->size = 0;
    child->allocated = 0;
    self->size++;
}

void syntreeNodeHelfer(Syntree *self, SyntreeNodeID list, SyntreeNodeID elem, int offset) {
    if (self->size == 0) {
        return;
    }

    int index = -1;
    for (int i = 0; i < self->size; i++) {
        if (self[i].value == list) {
            index = i;
            break;
        }
    }
    if (index != -1) {
        if (self->size == self->allocated) {
            self->allocated = self->allocated * 2;
            Syntree *stack = (Syntree *) realloc(self->child, self->allocated * sizeof(Syntree));
            if (stack == NULL) {
                fprintf(stderr, "Speicherallozieren ist fehlgeschlagen");
                exit(1);
            }
            self->child = stack;
        }
        insert(self, elem, index + offset);
        return;
    }
    for (int i = 0; i < self->size; i++) {
        syntreeNodeHelfer(&self[i], list, elem, offset);
    }
}



SyntreeNodeID syntreeNodeAppend(Syntree *self, SyntreeNodeID list, SyntreeNodeID elem){
    syntreeNodeHelfer(self, list, elem, 1);
    return list;
}

SyntreeNodeID syntreeNodePrepend(Syntree *self, SyntreeNodeID elem, SyntreeNodeID list){
    syntreeNodeHelfer(self, list, elem, 0);
    return elem;
}

void syntreePrintHelper(const Syntree *self, SyntreeNodeID root) {
    if (self == NULL)
        return;
    bool printed = false;
    if (self->value == root) {
        printf("(%i)", self->value);
        printed = true;
    }

    if (self->size == 0)
        return;
    if (printed)
        printf("{");
    for (int i = 0; i < self->size; i++) {
        Syntree tree = self[i];
        if (printed) {
            syntreePrintHelper(&tree, tree.value);
        } else {
            syntreePrintHelper(&tree, root);
        }
    }
    if (printed)
        printf("}");
}

void syntreePrint(const Syntree *self, SyntreeNodeID root){
    printf("{");
    syntreePrintHelper(self, root);
    printf("}");
}
