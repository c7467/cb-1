#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
/*
// Gruppe 20, Compilerbau - Monobachelor Informatik
// Anar Aliyev      (617945)
// Angelique Anders (613527)
*/

int stackInit(IntStack* self) {
    self->next = (int*)malloc(sizeof(int));
    if (self->next == NULL) {
        fprintf(stderr, "Speicher voll!");
        free(self->next);
        return 1;
    }else{
        self->head =-1;
        self->s = 0;
        return 0;
    }    
}

void stackRelease(IntStack* self) {
    free(self->next);
}


void stackPush(IntStack* self, int i) {       
        if (self->s == self->head) {
        self->head = self->head * 2;
        int *stack = (int *) realloc(self->next, self->head * sizeof(int));
        if (stack == NULL) {
            fprintf(stderr, "Allozieren ist fehlgeschlagen");
            exit(1);
        }
        self->next = stack;
    }
    self->next[self->s++] = i;
}

int stackTop(const IntStack* self) {
    if (stackIsEmpty(self) == 1)
    {  printf("IntStack ist leer!");
        return -1;
    }else{
        return self->next[self->head];
    }
}

int stackPop(IntStack* self) {
    if (stackIsEmpty(self) == 1)
    { printf("IntStack ist leer!");
        return -1;
    }
    else{
        return self->next[self->head--];
    }
}

int stackIsEmpty(const IntStack* self) {
    
    if (self->head == -1){
        return 1;
    }
    else{
        return 0;
    };
}

void stackPrint(const IntStack* self) {
for (int i = self->s-1; i >= 0; i--) {
        printf("%i\n", self->next[i]);
    }
}
